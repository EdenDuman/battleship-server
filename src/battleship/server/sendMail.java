package battleship.server;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class sendMail {

    public static boolean sendEmailTo(String mail, String password, String name) {

        try {
            System.out.println("Trying to send an email");
            Properties properties = new Properties();

            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "587");

            String myUserName = "EdenDuman8@gmail.com";
            String myPassword = "Eden112345";

            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(myUserName, myPassword);
                }
            });
            System.out.println();

            Message message = prepareMessage(session, myUserName, mail, password, name);

            Transport.send(message);
            System.out.println("Message sent successfully");

            return true;
        } catch (Exception ex) {
            System.out.println(" -> " + ex);
            return false;
        }
    }

    public static Message prepareMessage(Session session, String myUserName,
            String mail, String password, String name) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myUserName));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(mail));
            message.setSubject("מערכת משחק צוללות");

            String msg = "שלום " + name + " שלחנו לך קוד לשחזור הסיסמא" + 
                    "\n" + password;
            message.setText(msg);
            return message;
        } catch (Exception ex) {
            //Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(" -> " + ex);
        }
        return null;
    }
    
}
