
/**
 * @author Eden Duman 
 * יד 6 ברקאי
 * @version 1.1
 */
package battleship.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class BattleshipServer {

    private int port;
    private int id;
    private ServerSocket socketOfServer;
    private Socket socketOfClient;
    private ExecutorService pool;
    private int activeClients;
    
    // check using another class with samuel
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    public static void main(String[] args) throws IOException {

        // Preparing the database
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            System.out.println("database works !!!");
        } catch (ClassNotFoundException ex) {
            System.out.println("error: in database");
            System.exit(0);
        }

        // run the server
        // to run the server Remove the footnote(startServer) and add note to test func
        BattleshipServer serverobj = new BattleshipServer(5000, 5);
        serverobj.startServer();
        
        // test func
        //serverobj.test();
    }

    // Initializes the server
    BattleshipServer(int port, int maxConnections) {
        this.port = port;
        this.pool = Executors.newFixedThreadPool(maxConnections);
        this.id = 0;
        this.activeClients = 0;
        this.socketOfClient = null;
        this.socketOfServer = null;
    }

    // run the server
    private void startServer() throws IOException {

        // Uploading server
        this.socketOfServer = new ServerSocket(this.port);
        System.out.println("Server in the air !!!");

        // Disconnecting fallen users
        Thread t1 = new Thread(new automaticRunning());
        t1.start();
        
        // run full game
        //test();
        
        while (true) {
            
            // get new client
            socketOfClient = socketOfServer.accept();

            this.id += 1;
            this.activeClients += 1;

            // Send a request for treatment
            ServerThread runnable = new ServerThread(socketOfClient, id, this);
            pool.execute(runnable);

            //System.out.println("Connection " + id 
                    //+ " established with client " + socketOfClient);
        }
    }

    public void closeClient() {

        this.activeClients -= 1;
    }

    private void test(){
        SQLConnection sql = new SQLConnection();
        
        // Global data
        String ip = "80.246.141.254";
        String mac = "4E-95-09-F5-85-BF";
        int gameID = 0;
        String resultString = "";
        int resultInt = 0;
        String gameBoard = "D6:E2:E4:E6:E8:F1:F2:F4:F6:F8:G1:G4:G6:G8:H1:H4:H6:H8:";
        
        ///*/ Private data
        String mailOfUser1 = "EdenDuman7@gmail.com";
        String passwordOfUser1 = "FBFB9";
        int idOfUser1 = 0;
        
        String mailOfUser2 = "samuel.martiano1@gmail.com";
        String passwordOfUser2 = "19440";
        int idOfUser2 = 0;
        
        // If the user is already logged in, you will not be able to login
        resultString = sql.login(mailOfUser1, passwordOfUser1, ip, mac);
        idOfUser1 = Integer.parseInt(resultString);
        System.out.println("LoginUser1: " + ANSI_GREEN + idOfUser1 + ANSI_RESET);

        resultString = sql.login(mailOfUser2, passwordOfUser2, ip, mac);
        idOfUser2 = Integer.parseInt(resultString);
        System.out.println("LoginUser2: " + ANSI_RED + idOfUser2 + ANSI_RESET);
        
        // find game
        resultString = sql.findGame(idOfUser1)[0];
        sql.findGame(idOfUser2);
        
        gameID = Integer.parseInt(resultString);
        
        System.out.println( ANSI_GREEN + "gameID: " + ANSI_RED + gameID + ANSI_RESET);
        
        // set the location
        sql.setBoard(idOfUser1, gameID, gameBoard);
        sql.setBoard(idOfUser2, gameID, gameBoard);
        
        // user 1 hit and 2 miss
        String[] allHit =  gameBoard.split(":");
        for(int i = 0 ; i < allHit.length ; i++){
            
            resultInt = sql.setMove(idOfUser1, gameID, "A3");
            System.out.println("setMove: " + ANSI_GREEN + "user1 " +
                    checkResult(resultInt) + ANSI_RESET);
            
            resultInt = sql.setMove(idOfUser2, gameID, allHit[i]);
            System.out.println("setMove: " + ANSI_RED + "user2 " + 
                    checkResult(resultInt) + ANSI_RESET);
        }

        // get the msg of loss to user 1
        resultString = sql.checkGameUpdates(idOfUser1, gameID);
        System.out.println(ANSI_GREEN + "user1: " 
                + checkResult(Integer.parseInt(resultString)) + ANSI_RESET);
        
        // logout the players
        sql.logout(idOfUser1, ip, mac);
        sql.logout(idOfUser2, ip, mac);
    }

    // convert result from set move to msg 
    public String checkResult(int result){
        
        
        switch(result){
            case -1:
                return "los";
            case 0:
                return "miss";
            case 1:
                return "hit";
            case 2:
                return "win";
        }
        return "";
    }
}
