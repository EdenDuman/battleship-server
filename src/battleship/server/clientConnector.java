
/**
 * this class takes data from the client and returns him an answer
 * All functions take parameters from the client and call the
 * object of the database
 * @author Eden
 * @version 1.0
 */

package battleship.server;

import encryptionapi.Encryption;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;


public class clientConnector {

    private SQLConnection SQL;
    private BufferedReader reader;
    private PrintStream cout;
    private Socket socketOfClient;
    private BattleshipServer server;
    private int actionID;

    /**
     * 
     * @param st 
     */
    public clientConnector(ServerThread st) {
        this.SQL = new SQLConnection();
        this.cout = st.getCout();
        this.reader = st.getReader();
        this.socketOfClient = st.getSocketOfClient();
        this.server = st.getServer();
        this.actionID = st.getActionID();
    }

    /**
     * close the connection
     */
    public void close() {

        try {
            
            this.reader.close();
            this.cout.close();
            
            if(socketOfClient.isClosed()){
                this.socketOfClient.close();
            }
            
            server.closeClient();

        } catch (IOException ex) {
            System.out.println("Action(" + actionID + ") : error in close");
            System.out.println(" -> " + ex);
        }

        //System.exit(0);
    }

    public String getStringFromClient() {

        String str = "";
        try {
            str = reader.readLine();
            str = Encryption.open(str);
        } catch (IOException ex) {
            str = "-1";
            close();
            System.out.println("Action(" + actionID + ") : error in get String");
        }
        return str;
    }

    public int getIntFromClient() {

        String str = "";
        int mag = 0;
        try {
            str = reader.readLine();
            str = Encryption.open(str);
            mag = Integer.parseInt(str);
        } catch (IOException ex) {
            mag = -1;
            close();
            System.out.println("Action(" + actionID + ") : error in get int");
        }
        return mag;
    }

    public void setDataToClient(String str) {
        str = Encryption.close(str);
        cout.println(str);
        cout.flush();
    }

    public void registrar() {

        String result = "";
        
        String ip   = getStringFromClient().trim();
        String mac  = getStringFromClient().trim();
        String name = getStringFromClient().trim();
        String mail = getStringFromClient().trim();
        String pass = getStringFromClient().trim();

        result = SQL.Register(ip, mac, name, mail, pass);

        setDataToClient(result);

    }

    public void logout() {

        String result = "";

        int ID = getIntFromClient();
        String ip = getStringFromClient().trim();
        String mac = getStringFromClient().trim();

        result = SQL.logout(ID, ip, mac);

        setDataToClient(result);

    }

    public void login() {

        String result = "";

        String mail = getStringFromClient().trim();
        String password = getStringFromClient().trim();
        String ip = getStringFromClient().trim();
        String mac = getStringFromClient().trim();
        
        result = SQL.login(mail, password, ip, mac);
        
        // use a new way to run a proc
        /*
        ArrayList<String> data = new ArrayList<>();
        data.add(mail);
        data.add(password);
        data.add(ip);
        data.add(mac);
        
        result = SQL.test("P_login ?, ?, ?, ?", data).get(0);
        */
        setDataToClient(result);

    }

    public void findGame() {

        String[] result = new String[2];

        int ID = getIntFromClient();

        result = SQL.findGame(ID);

        setDataToClient(result[0]);
        setDataToClient(result[1]);

    }

    public void setMyBoard() {

        String result = "";

        int userID = getIntFromClient();
        int gameID = getIntFromClient();
        String board = getStringFromClient();

        result = SQL.setBoard(userID, gameID, board);

        setDataToClient(result);

    }

    public void resetPassword() {

        String result[] = new String[2];

        String mail = getStringFromClient();
        String ip = getStringFromClient();
        String mac = getStringFromClient();

        result = SQL.resetPassword(mail, ip, mac);

        if (result[0].length() != 5) {
            setDataToClient(result[0]);
        } else {
            if (sendMail.sendEmailTo(mail, result[0], result[1])) {
                setDataToClient("1");
            } else {
                setDataToClient("-2");
            }
        }
    }

    public void hello() {

        String result = "";

        int userID = getIntFromClient();

        result = SQL.updateLastSeen(userID);

        setDataToClient(result);
    }

    public void setMove() {

        int result = 0;

        int userID = getIntFromClient();
        int gameID = getIntFromClient();
        String move = getStringFromClient();

        result = SQL.setMove(userID, gameID, move);

        setDataToClient(result + "");
    }

    public void LookForAPartner() {

        String result = "";

        int gameID = getIntFromClient();

        result = SQL.LookForAPartner(gameID);

        setDataToClient(result);
    }

    public void checkGameUpdates() {

        String result = "";

        int userID = getIntFromClient();
        int gameID = getIntFromClient();

        result = SQL.checkGameUpdates(userID, gameID);

        setDataToClient(result);
    }

    public void getNickName() {
        
        String result = "";

        int userID = getIntFromClient();

        result = SQL.getNickName(userID);
        setDataToClient(result);
    }

    public void FinishedGame() {

        int gameID = getIntFromClient();

        SQL.FinishedGame(gameID);
    }
    
    public void hasTheGameStarted(){
        
        int result = -3;

        int userID = getIntFromClient();
        int gameID = getIntFromClient();

        result = SQL.hasTheGameStarted(userID, gameID);

        setDataToClient(result + "");   
    }
    
    public void top3Player(){
        
        ArrayList<String> result = new ArrayList<>();
        
        result = SQL.top3Player();
        
        for (String string : result) {
            setDataToClient(string + "");
        }
    }
    
    public void changePassword(){
        
        String result = "";
        
        int userID = getIntFromClient();
        String oldPassword = getStringFromClient();
        String newPassword = getStringFromClient();
        String ip = getStringFromClient();
        String mac = getStringFromClient();
        
        result = SQL.changePassword(userID, oldPassword, newPassword, ip, mac);
        
        setDataToClient(result);
        
    }

    public void timeOut(){
        
        String result = "";
        
        int userID = getIntFromClient();
        int gameID = getIntFromClient();
        
        result = SQL.timeOut(userID, gameID);
        
        setDataToClient(result);
        
    }
}
