package battleship.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * this class is in front of a client and directs his requests
 *
 * @author Eden
 */
public class ServerThread implements Runnable {

    private BattleshipServer server;
    private Socket socketOfClient;
    private BufferedReader reader;
    private PrintStream cout;
    private int actionID;
    private clientConnector connector;

    ServerThread(Socket client, int actionID, BattleshipServer server) throws IOException {
        this.server = server;
        this.socketOfClient = client;
        this.actionID = actionID;
        this.reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.cout = new PrintStream(client.getOutputStream());
        this.connector = new clientConnector(this);

    }

    @Override
    public void run() {

        // get operation
        String clientmsg;

        clientmsg = this.connector.getStringFromClient();

        System.out.print("Action(" + getActionID() + "): " + clientmsg + "\n");

        // Sorting requests
        operation(clientmsg);

        // close connection
        this.connector.close();

        System.out.println("Action(" + getActionID() + "): is over");

    }

    /**
     * Sorting requests
     *
     * @param operation Referral requests
     */
    private void operation(String operation) {

        switch (operation) {
            case "registrar":
                this.connector.registrar();
                break;
            case "logout":
                this.connector.logout();
                break;
            case "login":
                this.connector.login();
                break;
            case "findGame":
                this.connector.findGame();
                break;
            case "setMyBoard":
                this.connector.setMyBoard();
                break;
            case "resetPassword":
                this.connector.resetPassword();
                break;
            case "hello":
                this.connector.hello();
                break;
            case "setMove":
                this.connector.setMove();
                break;
            case "LookForAPartner":
                this.connector.LookForAPartner();
                break;
            case "checkGameUpdates":
                this.connector.checkGameUpdates();
                break;
            case "getNickName":
                this.connector.getNickName();
                break;
            case "FinishedGame":
                this.connector.FinishedGame();
                break;
            case "hasTheGameStarted":
                this.connector.hasTheGameStarted();
                break;
           case "top3Player":
                this.connector.top3Player();
                break;
           case "changePassword":
                this.connector.changePassword();
                break;
           case "timeOut":
               this.connector.timeOut();
               break;
            default:
                this.connector.setDataToClient("Not supported yet");
                System.out.println("error: the operation " + operation
                        + "not supported yet");
                break;
        }
    }

    /**
     * @return the server
     */
    public BattleshipServer getServer() {
        return server;
    }

    /**
     * @param server the server to set
     */
    public void setServer(BattleshipServer server) {
        this.server = server;
    }

    /**
     * @return the socketOfClient
     */
    public Socket getSocketOfClient() {
        return socketOfClient;
    }

    /**
     * @param socketOfClient the socketOfClient to set
     */
    public void setSocketOfClient(Socket socketOfClient) {
        this.socketOfClient = socketOfClient;
    }

    /**
     * @return the reader
     */
    public BufferedReader getReader() {
        return reader;
    }

    /**
     * @param reader the reader to set
     */
    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }

    /**
     * @return the cout
     */
    public PrintStream getCout() {
        return cout;
    }

    /**
     * @param cout the cout to set
     */
    public void setCout(PrintStream cout) {
        this.cout = cout;
    }

    /**
     * @return the actionID
     */
    public int getActionID() {
        return actionID;
    }

    /**
     * @param actionID the actionID to set
     */
    public void setActionID(int actionID) {
        this.actionID = actionID;
    }

    /**
     * @return the connector
     */
    public clientConnector getConnector() {
        return connector;
    }

    /**
     * @param connector the connector to set
     */
    public void setConnector(clientConnector connector) {
        this.connector = connector;
    }
}
